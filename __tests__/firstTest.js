import { functionThatNeedsToBeTested } from '../index';

global.test('a test', () => {
  expect(functionThatNeedsToBeTested(1)).toBe(1);
});
