run:
	npm start && node dist/index.js

jest:
	npx jest

test:
	node dist/test.js
